import { Theme, ThemeProvider } from '@fluentui/react';
import { Component, h } from 'preact';

export default class MediaQueryThemeProvider extends Component<{ themes: { dark: Theme, light: Theme } }, { isDark: boolean }> {
  _mediaQuery: MediaQueryList | null = null;
  state = { isDark: false };

  constructor() {
    super();
  }

  _onMediaQueryChange = (e: MediaQueryListEvent) => {
    this.setState({ isDark: e.matches });
  }

  componentDidMount = () => {
    this._mediaQuery = window.matchMedia('(prefers-color-scheme: dark)');
    this._mediaQuery.addEventListener('change', this._onMediaQueryChange);
    this.setState({ isDark: this._mediaQuery.matches });
  }

  componentWillUnmount = () => {
    if (this._mediaQuery != null) this._mediaQuery.removeEventListener('change', this._onMediaQueryChange);
  }

	render() {
		return (
			<ThemeProvider theme={this.state.isDark ? this.props.themes.dark : this.props.themes.light}>
        {this.props.children}
      </ThemeProvider>
		);
	}
}
