import { createTheme } from "@fluentui/react";

let lightTheme = createTheme({
  palette: {
    themePrimary: '#348529',
    themeLighterAlt: '#020502',
    themeLighter: '#081507',
    themeLight: '#10280c',
    themeTertiary: '#1f5019',
    themeSecondary: '#2e7524',
    themeDarkAlt: '#419137',
    themeDark: '#57a24d',
    themeDarker: '#7bba72',
    neutralLighterAlt: '#f8f8f8',
    neutralLighter: '#f4f4f4',
    neutralLight: '#eaeaea',
    neutralQuaternaryAlt: '#dadada',
    neutralQuaternary: '#d0d0d0',
    neutralTertiaryAlt: '#c8c8c8',
    neutralTertiary: '#111111',
    neutralSecondary: '#171616',
    neutralPrimaryAlt: '#1d1c1b',
    neutralPrimary: '#323130',
    neutralDark: '#282726',
    black: '#2d2c2c',
    white: '#ffffff',
  }
});

lightTheme.name = 'light';

export default lightTheme;