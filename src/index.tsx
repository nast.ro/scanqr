import App from './components/app';
import { registerIcons } from '@fluentui/react';
import { h, FunctionalComponent } from 'preact';
import * as Themes from './themes';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// initialize icons
import { faCameraRotate, faCheck, faCircleInfo, faCircleXmark, faCode, faFileArrowDown, faGear, faInfo, faLink, faQrcode, faShieldHalved, faUserShield, faXmark } from '@fortawesome/free-solid-svg-icons'
registerIcons({
  icons: {
    'camera-rotate': <FontAwesomeIcon icon={faCameraRotate} />,
    'check': <FontAwesomeIcon icon={faCheck} />,
    'circle-info': <FontAwesomeIcon icon={faCircleInfo} />,
    'circle-xmark': <FontAwesomeIcon icon={faCircleXmark} />,
    'code': <FontAwesomeIcon icon={faCode} />,
    'file-arrow-down': <FontAwesomeIcon icon={faFileArrowDown} />,
    'gear': <FontAwesomeIcon icon={faGear} />,
    'info': <FontAwesomeIcon icon={faInfo} />,
    'link': <FontAwesomeIcon icon={faLink} />,
    'qrcode': <FontAwesomeIcon icon={faQrcode} />,
    'shield-halved': <FontAwesomeIcon icon={faShieldHalved} />,
    'user-shield': <FontAwesomeIcon icon={faUserShield} />,
    'xmark': <FontAwesomeIcon icon={faXmark} />,

    // the following icons are used by fluentui components
    'checkmark': <FontAwesomeIcon icon={faCheck} />,
    'cancel': <FontAwesomeIcon icon={faXmark} />,
    'clear': <FontAwesomeIcon icon={faXmark} />,
    'errorbadge': <FontAwesomeIcon icon={faCircleXmark} />
  }
});

// add plausible analytics script if enabled
if (window.localStorage.getItem('allow-analytics') === 'true') {
  let analyticsScript = document.createElement('script');
  analyticsScript.src = 'https://plausible.io/js/plausible.js';
  analyticsScript.dataset.domain = window.location.hostname;
  document.head.appendChild(analyticsScript);
}

let themeWrappedApp: FunctionalComponent = () => {
  return (
    <Themes.MediaQueryThemeProvider themes={{ dark: Themes.dark, light: Themes.light }}>
      <App />
    </Themes.MediaQueryThemeProvider>
  );
};

// hide preload
let preload = document.getElementById('preload');
if (preload != null) preload.style.display = 'none';

export default themeWrappedApp;