import { mergeStyles, useTheme, MessageBar, MessageBarType, DialogType, PrimaryButton, DialogFooter, Dialog } from '@fluentui/react';
import { Component, Fragment, h, createRef } from 'preact';
import jsQR from 'jsqr';
import { QRCode } from 'jsqr';

import Header from './header';
import PrivacySetup from './privacy-setup';
import AppSetup from './app-setup';
import AboutPane from './about-pane';
import ScanResultContent from './scan-result-content';
import CameraPicker from './camera-picker';

export default class App extends Component<{}, { hash: string, permissionFailed: boolean, error: string | null, devices: MediaDeviceInfo[], activeDeviceId: string | null, scanResult: QRCode | null, scanStream: MediaStream | null }> {
  state = {
    hash: '',
    permissionFailed: false,
    error: null,
    devices: [] as MediaDeviceInfo[],
    activeDeviceId: null as string | null,
    scanResult: null,
    scanStream: null
  };

  _videoRef = createRef<HTMLVideoElement>();
  _canvas = document.createElement('canvas');
  _canvasCtx = this._canvas.getContext('2d');
  
  constructor() {
    super();
  }

  onError = (ex: any, operation: string | undefined = undefined) => {
    let prefix = '';
    if (typeof operation === 'string') prefix = `${operation} failed: `;
    console.error(ex);
    if (typeof ex === 'string') {
      this.setState({ error: `${prefix}${ex}` });
    } else if (ex instanceof Error) {
      this.setState({ error: `${prefix}${ex.message}` });
    } else if (ex === null) {
      this.setState({ error: `${prefix}null` });
    } else {
      this.setState({ error: `${prefix}${ex.toString()}` });
    }
  }

  onGlobalError = (e: ErrorEvent) => {
    console.error(e);
    this.onError(e.error);
  }

  onDismissErrorMessage = () => {
    this.setState({ error: null });
  }

  onReady = async (deviceId: string | undefined = undefined) => {
    if (this.state.scanStream === null || deviceId !== undefined) {
      try {
        if (deviceId === undefined) {
          let devices = await navigator.mediaDevices.enumerateDevices();
          this.state.devices = [] as MediaDeviceInfo[];
          for (let device of devices) if (device.kind === 'videoinput') this.state.devices.push(device);

          if (this.state.devices.length === 0) throw new Error('No video devices found');
          deviceId = this.state.devices[0].deviceId;
        }
        let stream = await navigator.mediaDevices.getUserMedia({ video: { deviceId } });
        this.setState({ permissionFailed: false, activeDeviceId: deviceId, scanStream: stream });
      } catch(err) {
        this.setState({ permissionFailed: true });
      }
    }
  }

  componentDidMount() {
    window.addEventListener('error', this.onGlobalError);
  }

  componentWillUnmount() {
    window.removeEventListener('error', this.onGlobalError);
  }

  scanTick = () => {
    if (this._videoRef.current !== null) {
      if (this._videoRef.current.readyState === this._videoRef.current.HAVE_ENOUGH_DATA) {
        if (this._canvasCtx == null) {
          this.onError('No canvas context', 'Scan');
          return;
        }
        this._canvas.height = this._videoRef.current.videoHeight;
        this._canvas.width = this._videoRef.current.videoWidth;
        this._canvasCtx.drawImage(this._videoRef.current, 0, 0, this._canvas.width, this._canvas.height);
        let imageData = this._canvasCtx.getImageData(0, 0, this._canvas.width, this._canvas.height);
        let code = jsQR(imageData.data, imageData.width, imageData.height, {
          inversionAttempts: "dontInvert",
        });
        if (code !== null) {
          this.setState({ scanResult: code });
          return; // stop scanning (skips requestAnimationFrame)
        }
      }
      requestAnimationFrame(this.scanTick);
    }
  }

  componentDidUpdate() {
    if (this._videoRef.current !== null) {
      if (this.state.scanStream !== null) {
        this._videoRef.current.srcObject = this.state.scanStream;
        this._videoRef.current.playsInline = true;
        this._videoRef.current.play();
        // do not attempt to scan if there already is a result
        if (this.state.scanResult === null) requestAnimationFrame(this.scanTick);
      } else {
        this._videoRef.current.srcObject = null;
      }
    }
  }

  render() {
    const theme = useTheme();

    return (
      <>
        <div
          className={mergeStyles(theme, {
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            minHeight: '100%',
            backgroundColor: theme.semanticColors.bodyBackground
          })}
        >
          <Dialog
            hidden={!this.state.permissionFailed}
            dialogContentProps={{
              type: DialogType.largeHeader,
              title: 'Permission denied',
              subText: 'This webapp requires access to your camera. Please allow access to your camera in your browser settings.',
            }}
          >
            <DialogFooter>
              <PrimaryButton onClick={() => this.onReady()} text="Retry" />
            </DialogFooter>
          </Dialog>
          <AboutPane />
          <AppSetup />
          <CameraPicker devices={this.state.devices} activeDeviceId={this.state.activeDeviceId === null ? undefined : this.state.activeDeviceId} onDeviceChange={this.onReady} />
          <PrivacySetup onBeforeRefresh={() => {
            return new Promise<void>((resolve, _) => {
              resolve();
            });
          }} onReady={this.onReady} />
          {this.state.error !== null &&
            <MessageBar
              className={mergeStyles(theme, {
                position: 'fixed',
                top: 50,
                left: 0,
                width: '100%',
                zIndex: 999
              })}
              messageBarType={MessageBarType.error}
              onDismiss={this.onDismissErrorMessage}
            >{this.state.error}</MessageBar>
          }
          <Header allowCameraSwap={this.state.scanResult === null && this.state.devices.length > 1} />
          <div className={mergeStyles(theme, {
            position: 'absolute',
            top: 50,
            left: 0,
            width: '100%',
            minHeight: 'calc(100% - 50px)',
            backgroundColor: theme.semanticColors.bodyBackground,
            display: 'flex',
            flexDirection: 'column'
          })}>
            <video
              ref={this._videoRef}
              muted={true}
              className={mergeStyles(theme, {
                height: 'calc(100vh - 50px)',
                display: this.state.scanResult === null ? 'initial' : 'none'
              })}
            />
            {this.state.scanResult !== null && <ScanResultContent content={this.state.scanResult} onDismiss={() => this.setState({ scanResult: null })} />}
          </div>
        </div>
      </>
    );
  }
}