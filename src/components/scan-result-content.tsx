import { CommandBar, ICommandBarItemProps, mergeStyles, useTheme } from '@fluentui/react';
import { Component, Fragment, h, createRef } from 'preact';
import { QRCode } from 'jsqr';
import monaco from 'monaco-editor';
import Editor from '@monaco-editor/react';
import globalState from '../global-state';

export default class ScanResultContent extends Component<{ content: QRCode, onDismiss: () => void }, {}> {
  constructor() {
    super();
  }

  _editorRef = createRef<monaco.editor.IStandaloneCodeEditor>();

  onSourceEditorMount = (editor: monaco.editor.IStandaloneCodeEditor, _: any) => {
    editor.updateOptions({ wordWrap: 'on' });
    editor.setValue(this.props.content.data);
    this._editorRef.current = editor;
  }

  onResize = () => {
    if (this._editorRef !== null) this._editorRef.current?.layout({} as monaco.editor.IDimension);
  }

  componentDidMount() {
    window.addEventListener('resize', this.onResize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onResize);
  }

  render() {
    const theme = useTheme();

    let commandBarItems: ICommandBarItemProps[] = [];

    try {
      // attempt to convert this.props.content.data to URL.
      // if succesful, then it's a url, therefore add a command bar item to open it.
      // if it fails, execution will move to the empty catch block and the item will not be added.
      let _ = new URL(this.props.content.data);
      commandBarItems.push({
        key: 'open-url',
        text: 'Open link',
        iconProps: { iconName: 'link' },
        onClick: () => {
          if (globalState.openInNewTab) window.open(this.props.content.data, '_blank');
          else window.location.href = this.props.content.data;
        }
      });
    } catch (ex) {}

    commandBarItems.push({
      key: 'download',
      text: 'Download content',
      iconProps: { iconName: 'file-arrow-down' },
      onClick: () => {
        const contentBlob = new Blob([ new Uint8Array(this.props.content.binaryData) ], { type: 'application/octet-stream' });
        const url = URL.createObjectURL(contentBlob);
        const link = document.createElement('a');
        link.href = url;
        link.download = 'content.txt';
        link.click();
        URL.revokeObjectURL(url);
      }
    });

    return (
      <>
        <div className={mergeStyles(theme, {
          display: 'flex',
          flexDirection: 'column',
          flexGrow: 1
        })}>
          <CommandBar
            items={commandBarItems}
            overflowButtonProps={{
              ariaLabel: 'More actions'
            }}
            farItems={[
              {
                key: 'dismiss',
                text: 'Scan again',
                iconProps: { iconName: 'qrcode' },
                iconOnly: true,
                onClick: this.props.onDismiss
              }
            ]}
            ariaLabel='Actions'
            primaryGroupAriaLabel='Actions for scanned code'
            farItemsGroupAriaLabel='Additional actions'
          />
          <Editor wrapperProps={{className: mergeStyles(theme, { flexGrow: 1 })}} height={0} onMount={this.onSourceEditorMount} options={{ readOnly: true }} theme={theme.name === 'dark' ? 'vs-dark' : 'vs'} />
        </div>
      </>
    );
  }
}