import { Checkbox, Panel, PrimaryButton, mergeStyles, useTheme, SpinButton } from '@fluentui/react';
import { Component, Fragment, h } from 'preact';
import globalState from '../global-state';
import { isPwa } from '../util';

export default class AppSetup extends Component<{}, { visible: boolean }> {
  state = {
    visible: false
  };

  constructor() {
    super();
  }

  onHashChange = () => {
    let hash = window.location.hash;
    if (hash.startsWith('#')) hash = hash.substring(1);
    this.setState({ visible: hash.toLowerCase() === 'setup' });
  }

  componentDidMount() {
    window.addEventListener('hashchange', this.onHashChange);
    this.onHashChange();
  }

  componentWillUnmount() {
    window.removeEventListener('hashchange', this.onHashChange);
  }

  onOpenInNewTabChange = (_: any, checked: boolean | undefined) => {
    if (typeof checked === 'boolean') {
      globalState.openInNewTab = checked;
      window.localStorage.setItem('open-in-new-tab', checked ? 'true' : 'false');
    }
    this.setState({});
  }

  onDismiss = () => {
    window.history.back();
  }

  renderFooter = () => {
    return (
      <>
        <PrimaryButton onClick={this.onDismiss}>Close</PrimaryButton>
      </>
    );
  }

  render() {
    const theme = useTheme();

    const checkboxStyle = mergeStyles(theme, {
      paddingTop: 10,
      paddingBottom: 10
    });

    return (
      <Panel
        headerText='Settings'
        isOpen={this.state.visible}
        onDismiss={this.onDismiss}
        onRenderFooterContent={this.renderFooter}
      >
        <>
          <Checkbox label="Open links in new tab" checked={globalState.openInNewTab} onChange={this.onOpenInNewTabChange} disabled={isPwa()} className={checkboxStyle} />
        </>
      </Panel>
    );
  }
}