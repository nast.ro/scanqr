import { Panel, PrimaryButton, mergeStyles, useTheme, ChoiceGroup, IChoiceGroupOption } from '@fluentui/react';
import { Component, Fragment, h } from 'preact';

export default class CameraPicker extends Component<{ devices: MediaDeviceInfo[], activeDeviceId: string | undefined, onDeviceChange: (deviceId: string) => void }, { visible: boolean }> {
  state = {
    visible: false
  };

  constructor() {
    super();
  }

  onHashChange = () => {
    let hash = window.location.hash;
    if (hash.startsWith('#')) hash = hash.substring(1);
    this.setState({ visible: hash.toLowerCase() === 'cameras' });
  }

  componentDidMount() {
    window.addEventListener('hashchange', this.onHashChange);
    this.onHashChange();
  }

  componentWillUnmount() {
    window.removeEventListener('hashchange', this.onHashChange);
  }

  onDismiss = () => {
    window.history.back();
  }

  renderFooter = () => {
    return (
      <>
        <PrimaryButton onClick={this.onDismiss}>Close</PrimaryButton>
      </>
    );
  }

  onChanged = (_: any, option: IChoiceGroupOption | undefined) => {
    if (option !== undefined) this.props.onDeviceChange(option.key);
  }

  render() {
    // swap to the only other camera if there are only two
    if (this.state.visible && this.props.devices.length === 2) {
      let other = this.props.devices.find(d => d.deviceId !== this.props.activeDeviceId);
      if (other !== undefined) {
        this.props.onDeviceChange(other.deviceId);
        window.history.back();
        this.state.visible = false;
      }
    }

    return (
      <Panel
        headerText='Switch camera'
        isOpen={this.state.visible}
        onDismiss={this.onDismiss}
        onRenderFooterContent={this.renderFooter}
      >
        <ChoiceGroup
          selectedKey={this.props.activeDeviceId}
          onChange={this.onChanged}
          options={this.props.devices.map((device) => {
            return {
              key: device.deviceId,
              text: device.label
            };
          })}
        />
      </Panel>
    );
  }
}