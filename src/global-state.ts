import { isPwa } from "./util";

type GlobalState = {
  openInNewTab: boolean,
}

let globalState = {
  // open in new tab should be false by default & forced to true if running as a pwa
  openInNewTab: window.localStorage.getItem('open-in-new-tab') === 'true' || isPwa(),
}

export type { GlobalState };

export default globalState;