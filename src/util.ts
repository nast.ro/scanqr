export function isPwa() {
  return window.matchMedia('(display-mode: standalone), (display-mode: minimal-ui)').matches;
}